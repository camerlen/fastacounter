# FastaCounter

A script to count the number of sequences in a collection of FASTA files.

You can test the script on the example FASTA file: **example.fasta**, which has 4 sequences:

```bash
$ ./count_sequences.sh example.fasta
```

Which should give the output:

```bash
example.fasta: 4
```

